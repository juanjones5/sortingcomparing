import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {

    public static void main(String[] args) throws IOException {
        int t= 50000;
        int t2=100000;
        int t3=200000;
        int t4=300000;
        int t5=400000;

        int size = 0;
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        String inputString;
        System.out.println("******* COMPARE SORTING ALGORITHMS *******");

        //STEP ONE: SELECT THE SIZE OF THE ARRAY
        while (true) {
            System.out.println();
            System.out.println("Type the size of the array you want to sort");
            try {
                inputString = inputReader.readLine();
                size = Integer.parseInt(inputString);
                break;
            } catch (NumberFormatException error) {
                System.out.println("Please enter an integer"); //if the input is not an integer, the program will ask again for an integer
                continue;
            }
        }

        int [] numbers = new int [size];

        while (true) {
            System.out.println("Select the type of array you would like to sort");
            System.out.println("\tType 1 for an array already sorted in descending order");
            System.out.println("\tType 2 for an array with all the same data");
            System.out.println("\tType 3 for an array sorted in ascending order");
            System.out.println("\tType 4 for an array in random order");

            try {
                inputString = inputReader.readLine();
                int choice = Integer.parseInt(inputString);
                int j = size - 1;
                switch (choice) {
                    case 1:
                        int k;
                        // we are allowing each value to have a 20% chance of being a duplicate value
                        for (int i=0; i<size; i++){
                            k = (int)(Math.random() * 5) + 1;
                            if (k!=2)
                                numbers[i] = j--;
                            else
                                numbers[i] = j+1;
                        }
                        break;
                    case 2:
                        j = (int)(Math.random() * 1000) + 1;
                        for (int i=0; i<size; i++){
                            numbers[i] = j;
                        }
                        break;
                    case 3:
                        for (int i=0; i<size; i++){
                            j = (int)(Math.random() * 5) + 1;
                            // we are allowing each value to have a 20% chance of being a duplicate value
                            if (j!=2)
                                numbers[i] = i;
                            else
                                numbers[i] = i - 1;
                        }
                        break;
                    case 4:
                        j = (int)(Math.random() * 1000) + 1;
                        for (int i=0; i<size; i++){
                            j = (int)(Math.random() * 1000) + 1;
                            numbers[i] = j;
                        }
                        break;
                }
                break;
            } catch (NumberFormatException error) {
                System.out.println("Please enter an integer"); //if the input is not an integer, the program will ask again for an integer
                continue;
            }
        }

        System.out.println();
        System.out.println("Your random selected list before ordering is: ");
        for (int i = 0; i<size; i++){
            System.out.print(numbers[i] + ", ");
        }
        System.out.println();

        System.out.println("INSERTION SORT");
        Long start = System.currentTimeMillis();
        InsertionSort.sort(numbers);
        Long finish = System.currentTimeMillis();
        System.out.println("Insertion Sort has taken " + (finish-start) + " milliseconds");

        System.out.println("MERGESORT SORT");
        start = System.currentTimeMillis();
        Mergesort.sort(numbers);
        finish = System.currentTimeMillis();
        System.out.println("Mergesort has taken " + (finish-start) + " milliseconds");

        System.out.println("HEAPSORT");
        start = System.currentTimeMillis();
        Heapsort.sort(numbers);
        finish = System.currentTimeMillis();
        System.out.println("Mergesort has taken " + (finish-start) + " milliseconds");

        /**
         * QUICK SORT
         */
//        System.out.println("***********Quick Sort *****************");
//        Long b=System.currentTimeMillis();
//        QuickSort q1=new QuickSort(t);
//        q1.sort();
//        Long a=System.currentTimeMillis();
//        System.out.println("Quick sort takes "+(a-b)+" ms"+ " for " +t);
//        Long b2=System.currentTimeMillis();
//        QuickSort q2=new QuickSort(t2);
//        q2.sort();
//        Long a2=System.currentTimeMillis();
//        System.out.println("Quick sort takes "+(a2-b2)+" ms"+ " for " +t2);
//        Long b3=System.currentTimeMillis();
//        QuickSort q3=new QuickSort(t3);
//        q3.sort();
//        Long a3=System.currentTimeMillis();
//        System.out.println("Quick sort takes "+(a3-b3)+" ms"+ " for "+ t3);
//        Long b4=System.currentTimeMillis();
//        QuickSort q4=new QuickSort(t4);
//        q4.sort();
//        Long a4=System.currentTimeMillis();
//        System.out.println("Quick sort takes "+(a4-b4)+" ms" + " for "+ t4);
//        Long b5=System.currentTimeMillis();
//        QuickSort q5=new QuickSort(t5);
//        q5.sort();
//        Long a5=System.currentTimeMillis();
//        System.out.println("Quick sort takes "+(a5-b5)+" ms" + " for "+ t5);
//
//
//        /**
//         * InsertionSort
//         */
//        System.out.println("********************Insertion Sort ************************");
//        b=System.currentTimeMillis();
//        InsertionSort i1=new InsertionSort(t);
//        i1.sort();
//        a=System.currentTimeMillis();
//        System.out.println("Insertion sort takes "+(a-b)+ " ms"+ " for " +t);
//        b2=System.currentTimeMillis();
//        InsertionSort i2=new InsertionSort(t2);
//        i2.sort();
//        a2=System.currentTimeMillis();
//        System.out.println("Insertion sort takes "+(a2-b2)+" ms"+ " for " +t2);
//        b3=System.currentTimeMillis();
//        InsertionSort i3=new InsertionSort(t3);
//        i3.sort();
//        a3=System.currentTimeMillis();
//        System.out.println("Insertion sort takes "+(a3-b3)+" ms"+ " for "+ t3);
//        b4=System.currentTimeMillis();
//        InsertionSort i4=new InsertionSort(t4);
//        i4.sort();
//        a4=System.currentTimeMillis();
//        System.out.println("Insertion sort takes "+(a4-b4)+" ms" + " for "+ t4);
//        b5=System.currentTimeMillis();
//        InsertionSort i5=new InsertionSort(t5);
//        i5.sort();
//        a5=System.currentTimeMillis();
//        System.out.println("Insertion sort takes "+(a5-b5)+" ms" + " for "+ t5);
//
//        /**
//         * MergeSort
//         */
//        System.out.println("*******************MergeSort************************");
//        MergeSort m1=new MergeSort(t);
//        m1.sort();
//        a=System.currentTimeMillis();
//        System.out.println("Merge sort takes "+(a-b)+ " ms"+ " for " +t);
//        b2=System.currentTimeMillis();
//        MergeSort m2=new MergeSort(t2);
//        m2.sort();
//        a2=System.currentTimeMillis();
//        System.out.println("Merge sort takes "+(a2-b2)+ " ms"+ " for " +t2);
//        b3=System.currentTimeMillis();
//        MergeSort m3=new MergeSort(t3);
//        m3.sort();
//        a3=System.currentTimeMillis();
//        System.out.println("Merge sort takes "+(a3-b3)+" ms"+ " for "+ t3);
//        b4=System.currentTimeMillis();
//        MergeSort m4=new MergeSort(t4);
//        m4.sort();
//        a4=System.currentTimeMillis();
//        System.out.println("Mege sort takes "+(a4-b4)+ " ms" + " for "+ t4);
//        b5=System.currentTimeMillis();
//        MergeSort m5=new MergeSort(t5);
//        m5.sort();
//        a5=System.currentTimeMillis();
//        System.out.println("Merge sort takes "+(a5-b5)+" ms" + " for "+ t5);
//
//        /**
//         * SELECTION SORT
//         */
//
//        System.out.println("********************Selection Sort****************");
//        b=System.currentTimeMillis();
//        SelectionSort s1=new SelectionSort(t);
//        s1.sort();
//        a=System.currentTimeMillis();
//        System.out.println("Selection sort takes "+(a-b)+" ms"+ " for " +t);
//        b2=System.currentTimeMillis();
//        SelectionSort s2=new SelectionSort(t2);
//        s2.sort();
//        a2=System.currentTimeMillis();
//        System.out.println("Selection sort takes "+(a2-b2)+ "ms"+ " for " +t2);
//        b3=System.currentTimeMillis();
//        SelectionSort s3=new SelectionSort(t3);
//        s3.sort();
//        a3=System.currentTimeMillis();
//        System.out.println("Selection sort takes "+(a3-b3)+" ms"+ " for "+ t3);
//        b4=System.currentTimeMillis();
//        SelectionSort s4=new SelectionSort(t4);
//        s4.sort();
//        a4=System.currentTimeMillis();
//        System.out.println("Selection sort takes "+(a4-b4)+" ms" + " for "+ t4);
//        b5=System.currentTimeMillis();
//        SelectionSort s5=new SelectionSort(t5);
//        s5.sort();
//        a5=System.currentTimeMillis();
//        System.out.println("Selection sort takes "+(a5-b5)+" ms" + " for "+ t5);

    }
}


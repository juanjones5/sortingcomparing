public class InsertionSort {

    public static void sort(int[] a)
    {
        //sort a[] into increasing order
        int n = a.length;
        for (int i = 1; i<n; i++)
        {
            for (int j = i; j>0 && a[j]<a[j-1]; j--)
                exchange(a, j, j-1);
        }
    }

    private static boolean less(Comparable v, Comparable w) {

        return v.compareTo(w) < 0;
    }

    // exchange a[i] and a[j]
    private static void exchange(Object[] a, int i, int j) {
        Object swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    // exchange a[i] and a[j]  (for indirect sort)
    private static void exchange(int[] a, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

}
